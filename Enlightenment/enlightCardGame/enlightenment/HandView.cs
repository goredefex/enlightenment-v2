﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Enlightenment_Presentation {
    public class HandView : RectangleView {

        //Hand Placing
        private const int numberOfCards = 9;
                       
        //Hand Sizing
        private const int handWidth = 80;
        private const int handheight = 114;
        private int spacingSize = 5;
        

        // =====================================
        //Constructs ---------------------------
        // =====================================

        public HandView() { //<-- Default
            this.setX(0);
            this.setY(0);
            this.setWidth(handWidth);
            this.setHeight(handheight);
            this.ChangeRectColor(Color.ForestGreen);
        
        } //end construct

        //Takes X,Y Coordinates
        public HandView(int newX, int newY) {
            this.setX(newX);
            this.setY(newY);
            this.setWidth(handWidth);
            this.setHeight(handheight);
            this.ChangeRectColor(Color.ForestGreen);
        
        } //end construct

        // =====================================


        // =====================================
        //Fields -------------------------------
        // =====================================
        
        /// <summary>
        /// Gets The Width of The Hand Holder
        /// </summary>
        public int GetHandWidth { get { return handWidth; } } //end function

        /// <summary>
        /// Gets The Height of The Hand Holder
        /// </summary>
        public int GetHandHeight { get {  return handheight; } } //end function

        /// <summary>
        /// Gets The Padding of The Hand Holder Sidings
        /// </summary>
        public int GetSpacingSize { get {  return spacingSize; } } //end function


        // =====================================
        //Functions ---------------------------
        // =====================================

        /// <summary>
        /// Draws The Hand Holder
        /// </summary>
        /// <param name="e">PaintEventArgs - Form Paint Event</param>
        public void DrawHandHolder(System.Windows.Forms.PaintEventArgs e) {
            this.MakeRectangle(e);
        
        } //end function

        // =====================================
    
    } //EOC

} //EON
