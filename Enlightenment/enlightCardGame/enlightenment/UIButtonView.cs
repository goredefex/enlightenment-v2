﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Enlightenment_Presentation {
    public class UIButtonView : RectangleView {


        //Button Sizing
        private const int buttonWidth = 100;
        private const int buttonHeight = 25;
        private const int txtLabelWidth = 40;
        private const int txtLabelHeight = 20;
        private const int labelPaddingTop = 3;
        private const int labelPaddingLeft = 33;

        //Button Control Vars
        private string btnText = String.Empty;

        //Decal Vars
        private Font font = new Font("Microsoft Sans Serif", 10, FontStyle.Bold);


        // =====================================
        //Constructs ---------------------------
        // =====================================

        //Takes Point Location Of Button & Text
        public UIButtonView(Point loc, string newText) {
            this.setX(loc.X);
            this.setY(loc.Y);
            this.setWidth(buttonWidth);
            this.setHeight(buttonHeight);
            this.btnText = newText;
            this.ChangeRectColor(Color.Black);
        
        } //end construct

        //Takes Integer X,Y Location(s) Of Button & Text
        public UIButtonView(int newX, int newY, string newText) {
            this.setX(newX);
            this.setY(newY);
            this.setWidth(buttonWidth);
            this.setHeight(buttonHeight);
            this.btnText = newText;
            this.ChangeRectColor(Color.Black);
                    
        } //end construct

        // =====================================


        // =====================================
        //Functions ----------------------------
        // =====================================

        /// <summary>
        /// Draws Given Button Settings
        /// Pre Set When Built or Manually
        /// Added
        /// </summary>
        /// <param name="e">PaintEventArgs - Form Paint Event</param>
        public void DrawButton(System.Windows.Forms.PaintEventArgs e) {
            this.MakeRectangle(e);
            this.MakeAndSetDecals(e);

        } //end event


        /// <summary>
        /// Makes & Sets Button Wording
        /// </summary>
        /// <param name="e">PaintEventArgs - Form Paint Event</param>
        public void MakeAndSetDecals(System.Windows.Forms.PaintEventArgs e) {
            this.ChangeWordColor(Color.White);
            this.MakeString(e, this.font, this.btnText, 
                (this.getX()+labelPaddingLeft), (this.getY()+labelPaddingTop));
        
        } //end function

        // =====================================


    } //EOC

} //EON
