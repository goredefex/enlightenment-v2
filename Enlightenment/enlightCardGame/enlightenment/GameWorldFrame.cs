﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using CardLibrary;
using Enlightenment_Business;

namespace Enlightenment_Presentation {
    public partial class GameWorldFrame : Form {

        //Important Strings
        private const string aiPlayedMsg = "An AI Player Has Played A Card... ";
        private const string aiCantGo = "AI Player Could Not Go, AI Drew A Card";
        private const string hitNextBtn = "To Proceed Click Next Button";
        private const string gameWon = "You Have Won The Game!";
        private const string gameLost = "You Lost. Restarting...";
        private const string roundWon = "You Have Won The Round! You Have Gained A Path Point";
        private const string aiRoundWon = "An AI won this round";
        private const string userCantGo = "You Have No Current Moves!";
        private const string userDraw = "Please Draw A Card";

        //Events
        public delegate void CardPlayedHandler();
        public event CardPlayedHandler TurnPlayed;
        public delegate void RoundWonHandler();
        public event RoundWonHandler RoundWon;
        
        //UI Locations
        private Point exitButtonLocation = new Point(10, 415);
        private Point resetButtonLocation = new Point(10, 385);
        private Point nextTurnButtonLocation = new Point(185, 415);
        private Point drawButtonLocation = new Point(300, 415);
        private Point messageBoxButtonLocation = new Point(10, 270);
        private Point handStartLocation = new Point(10, 450);

        //Mouse Control Vars
        private bool dragging = false;
        private Point currClick = Point.Empty;
        private int currCardClicked = -1;
        private bool gameOver = false;

        //Views
        private DiscardPileView discardZone = new DiscardPileView();
        private HandView[] handHolders;
        private UIButtonView exitButton;
        private UIButtonView resetButton;
        private UIButtonView nextTurnButton;
        private UIButtonView drawCardButton;
        private MessageView messageBox;
                
        //Game Objects
        private Gameplay game = new Gameplay();
        private CardView cardViewer;
        private CardView[] cards;


        // =====================================
        //Constructs ---------------------------
        // =====================================

        public GameWorldFrame() {
            InitializeComponent();
            //this.DoubleBuffered = true;
            this.SetStyle(
                  ControlStyles.AllPaintingInWmPaint |
                  ControlStyles.UserPaint |
                  ControlStyles.DoubleBuffer,true);
            this.UpdateStyles();
                        
            //Instantiate All UI Objects
            this.nextTurnButton = new UIButtonView(this.nextTurnButtonLocation, "Next");
            this.nextTurnButton.ChangeRectColor(Color.Crimson);
            this.drawCardButton = new UIButtonView(this.drawButtonLocation, "Draw");
            this.drawCardButton.ChangeRectColor(Color.YellowGreen);
            this.messageBox = new MessageView(this.messageBoxButtonLocation);
            this.exitButton = new UIButtonView(this.exitButtonLocation, "Exit");
            this.resetButton = new UIButtonView(this.resetButtonLocation, "Reset");

            this.handHolders = new HandView[this.game.GetNumberOfCards];
            this.cards = new CardView[this.game.GetNumberOfCards];

            //Give Reference Obj For Sizing
            this.handHolders[0] = new HandView();

            //Begin Filling UI
            this.SetAndPlaceHolders();
            this.FillCardViews();
                        
        } //end constructor

        // =============================================
        

        // =====================================
        //Functions ----------------------------
        // =====================================


        //Make Card Views For Hand & Place In Holders
        private void FillCardViews() {
            for(int i=0; i<this.game.player.hand.Length; i++) {
                if(this.game.player.hand[i]!=null) {
                    this.cards[i] = new CardView(this.game.player.hand[i],
                        this.handHolders[i].getX(), this.handHolders[i].getY());
                }
            }
        
        } //end function


        //Lays Down Staticly Placed Holder Views
        private void SetAndPlaceHolders() {
            for(int i=0; i<this.handHolders.Length; i++) {
                this.handHolders[i] = new HandView((handStartLocation.X+
                    (this.handHolders[0].GetSpacingSize*i)+(this.handHolders[0].getWidth()*i)), 
                        this.handStartLocation.Y);
            
            }
        
        } //end function


        //Card Dragging Event Control
        private void RemoveCardDragging() {
            this.MouseUp -= this.PlayArea_MouseUp;
            this.MouseMove -= this.PlayArea_MouseMove;
        
        } //end function


        //Card Dragging Event Control
        private void AddCardDragging() {
            this.MouseUp += this.PlayArea_MouseUp;
            this.MouseMove += this.PlayArea_MouseMove;
        
        } //end function

        
        //Paint Removal
        private void WipeScreen() {
            this.Paint -= this.ExitButton_Paint;
            this.Paint -= this.ResetButton_Paint;
            this.Paint -= this.NextButton_Paint;
            this.Paint -= this.DrawButton_Paint;
            this.Paint -= this.MessageBox_Paint;
            this.Paint -= this.DiscardPile_Paint;
            this.Paint -= this.DiscardStack_Paint;
            this.Paint -= this.HandHolders_Paint;
            this.Paint -= this.Cards_Paint;
        
        } //end function


        //Paint Replacement
        private void ItemsBackToScreen() {
            this.Paint += this.ExitButton_Paint;
            this.Paint += this.ResetButton_Paint;
            this.Paint += this.DiscardPile_Paint;
            this.Paint += this.DiscardStack_Paint;
            this.Paint += this.HandHolders_Paint;
            this.Paint += this.Cards_Paint;
        
        } //end function


        /// <summary>
        /// Takes A Card (usually
        /// From A CardView) &
        /// Places It On The Discard Pile(X,Y),
        /// Removes From Hand
        /// </summary>
        /// <param name="newCard">Card - Card To Put In Play</param>
        private void PlayAUserCard(Card newCard) {
            this.game.discards.Add(newCard);
            if(this.cardViewer==null) {
                //Remove Card Selected From Hand
                this.game.player.PlayCardFromUsersHand(this.currCardClicked);
                //Add Card To Discard Pile
                this.cardViewer = new CardView(newCard,
                    this.discardZone.GetPileX(), this.discardZone.GetPileY());
                //Remove Card From Hand Views
                this.cards[this.currCardClicked] = null;
                this.game.TakeUserTurn(); //<--Turn Duties
                TurnPlayed(); //<--Flag Finish

            } else {
                //Remove Old Viewer & Card Selected From Hand
                this.Paint -= this.DiscardStack_Paint;
                this.game.player.PlayCardFromUsersHand(this.currCardClicked);
                //Add Card To Discard Pile
                this.cardViewer = new CardView(newCard,
                    this.discardZone.GetPileX(), this.discardZone.GetPileY());
                //Remove Card From Hand Views & Repaint
                this.cards[this.currCardClicked] = null;
                this.Paint += this.DiscardStack_Paint;
                this.game.TakeUserTurn(); //<--Turn Duties
                TurnPlayed(); //<--Flag Finish
            
            }

        } //end function


        /// <summary>
        /// Takes An AI Card From aiHands 
        /// & Places It On The Discard Pile(X,Y),
        /// Removes From Hand
        /// </summary>
        /// <param name="newCard">Card - Card To Put In Play</param>
        private void PlayAICard(Card newCard) {
            if(newCard!=null) {
                this.game.discards.Add(newCard);
                //Remove Old Viewer
                this.Paint -= this.DiscardStack_Paint;
                //Add Card To Discard Pile
                this.cardViewer = new CardView(newCard, 
                    this.discardZone.GetPileX(), this.discardZone.GetPileY());
                //Set Message Box Msg
                this.messageBox.SetMessage(aiPlayedMsg + hitNextBtn);
                //Add Painting Back In
                this.Paint += this.DiscardStack_Paint;
                this.Paint += this.NextButton_Paint;
                this.Paint += this.MessageBox_Paint;
                //Update Notification Labels
                aiPlayLabel.Text = this.cardViewer.currCard.value.ToString();

            }
                        
        } //end function


        #region "Events"








        #endregion "Events"

        // =====================================
        //Events -------------------------------
        // =====================================

        //Load Event
        private void GameWorldFrame_Load(object sender, EventArgs e) {
            //Event Set
            this.TurnPlayed += new CardPlayedHandler(this.TurnPlayed_Event);

            //Paint Events
            this.Paint += this.ExitButton_Paint;
            this.Paint += this.ResetButton_Paint;
            this.Paint += this.DiscardPile_Paint;
            this.Paint += this.DiscardStack_Paint;
            this.Paint += this.HandHolders_Paint;
            this.Paint += this.Cards_Paint;
            //Added Design Events
            this.MouseDown += this.PlayArea_MouseDown;
            this.MouseUp += this.PlayArea_MouseUp;
            this.MouseMove += this.PlayArea_MouseMove;

            //Begin Constant Drawing
            frameRateTimer.Start();
            functionalityTimer.Start();
                                    
        } //end event

        
        //Card Has Been Played Event
        public void TurnPlayed_Event() {
            this.game.NextPersonsTurn();
            this.game.AdvanceTurnCounter();
            
            if(this.game.GetWhosTurn!=this.game.GetUserNumber) {
                //Card tempCard = this.game.TakeAITurn();

                //if(tempCard!=null)
                //    this.PlayAICard(tempCard);
                //else 
                //    this.messageBox.SetMessage(aiCantGo);
                //    this.Paint += this.MessageBox_Paint;
                //    this.Paint += this.NextButton_Paint;
                aiTimer.Start();
            } 

        } //end function

        
        //Card Paint Event 
        private void Cards_Paint(Object sender, PaintEventArgs e) {
            for(int i=0; i<this.game.player.hand.Length; i++) {
                if(this.cards[i]!=null) 
                    this.cards[i].DrawCard(e);
            }
             
        } //end event

        //Discard Pile Paint Event
        private void DiscardPile_Paint(Object sender, PaintEventArgs e) {
            this.discardZone.DrawDiscardPile(e);
        
        } //end event

        //Discard Stack Paint Event
        private void DiscardStack_Paint(Object sender, PaintEventArgs e) {
            if(this.cardViewer!=null) 
                this.cardViewer.DrawCard(e);
            
        
        } //end event

        //Next Button Paint Event
        private void NextButton_Paint(Object sender, PaintEventArgs e) {
            this.nextTurnButton.DrawButton(e);
        
        } //end event
        
        //Draw Button Paint Event
        private void DrawButton_Paint(Object sender, PaintEventArgs e) {
            this.drawCardButton.DrawButton(e);
        
        } //end event

        //Exit Button Paint Event
        private void ExitButton_Paint(Object sender, PaintEventArgs e) {
            this.exitButton.DrawButton(e);
        
        } //end event

        //Reset Button Paint Event
        private void ResetButton_Paint(Object sender, PaintEventArgs e) {
            this.resetButton.DrawButton(e);
        
        } //end event

        //Message Box Paint Event
        private void MessageBox_Paint(Object sender, PaintEventArgs e) {
            this.messageBox.DrawBox(e);
        
        } //end event

        //Hand Holder Paint Event
        private void HandHolders_Paint(Object sender, PaintEventArgs e) {
            for(int i=0; i<this.handHolders.Length; i++) {
                if(this.handHolders[i]!=null)
                    this.handHolders[i].DrawHandHolder(e);
            }
             
        } //end event


        
        /// <summary>
        /// Mouse Up Event - 
        /// Controls The Functionality
        /// Of All Button(s) Releasing &
        /// Card Dropping
        /// </summary>
        /// <param name="sender">object - Form Control Sender</param>
        /// <param name="e">MouseEventArgs - Form Mouse Event</param>
        private void PlayArea_MouseUp(object sender, MouseEventArgs e) { 
            if(this.currCardClicked<0)
                return;

            if(this.currCardClicked>=0) { //<--If Valid
                //If Within The Discard Pile View
                if(this.cards[this.currCardClicked].getRect().Right>=this.discardZone.getRect().Left &&
                    this.cards[this.currCardClicked].getRect().Left<=this.discardZone.getRect().Right &&
                        this.cards[this.currCardClicked].getRect().Bottom>=this.discardZone.getRect().Top &&
                            this.cards[this.currCardClicked].getRect().Top<=this.discardZone.getRect().Bottom) {

                    //If The Card Being Dropped Is Allowed
                    if(this.game.CheckIfCardLegal(this.cards[this.currCardClicked].currCard)) {
                        this.PlayAUserCard(this.cards[this.currCardClicked].currCard);//<--Play Card
                        this.currCardClicked = -1;
                       
                    //If Not Allowed
                    } else {
                        this.cards[this.currCardClicked].setX(this.handHolders[this.currCardClicked].getX());
                        this.cards[this.currCardClicked].setY(this.handHolders[this.currCardClicked].getY());
                        this.currCardClicked = -1;
                        cardPlayedDispLbl.Text = "None";
                    }
                
                //Not Dropped On Pile
                } else {
                    this.cards[this.currCardClicked].setX(this.handHolders[this.currCardClicked].getX());
                    this.cards[this.currCardClicked].setY(this.handHolders[this.currCardClicked].getY());
                    this.currCardClicked = -1;
                    cardPlayedDispLbl.Text = "None";
            
                }
            }
           
            this.dragging = false; //<--Flag dragging Off
        
        } //end event


        /// <summary>
        /// Mouse Down Event - 
        /// Controls The Functionality
        /// Of All Buttons Clicking &
        /// Card Dragging
        /// </summary>
        /// <param name="sender">object - Form Control Sender</param>
        /// <param name="e">MouseEventArgs - Form Mouse Event</param>
        private void PlayArea_MouseDown(object sender, MouseEventArgs e) {
            if(e.Button != MouseButtons.Left) //<--Left Click Catch
                return;

            //Exit Button Functionality
            if(this.exitButton.getRect().Contains(e.Location)) {
                Application.Exit();
            }

            //Reset Button Functionality
            if(this.resetButton.getRect().Contains(e.Location))
                Application.Restart();

            //Next Button Functionality
            if(!this.nextTurnButton.getRect().IsEmpty && this.nextTurnButton.getRect().Contains(e.Location)) {
                TurnPlayed();
            }

            //Draw Button Functionality
            if(!this.drawCardButton.getRect().IsEmpty && this.drawCardButton.getRect().Contains(e.Location)) {
                this.game.player.PutCardInHand(this.game.TopCard());
                this.FillCardViews();
                this.Paint -= this.DrawButton_Paint;
                this.Paint += this.NextButton_Paint;
                TurnPlayed();
            }
            
            //Look For Card In Click
            for(int i=0; i<this.cards.Length; i++) {
                if(this.cards[i]!=null) {
                    if(this.cards[i].CheckCursorInCard(e.Location)) {
                        this.dragging = true;
                        this.currCardClicked = i;
                        i = this.cards.Length;
                    } else {
                        this.currCardClicked = -1;
                    }
                }
            }

        } //end event
        
        
        /// <summary>
        /// Mouse Move Event - 
        /// Controls The Functionality
        /// Of All Cards Movement On Form
        /// </summary>
        /// <param name="sender">object - Form Control Sender</param>
        /// <param name="e">MouseEventArgs - Form Mouse Event</param>
        private void PlayArea_MouseMove(object sender, MouseEventArgs e) {
            if (e.Button == MouseButtons.Left) { 
                if(this.dragging) {
                    this.cards[this.currCardClicked].setX(e.X);
                    this.cards[this.currCardClicked].setY(e.Y);
                    
                    //If In The Discard Zone
                    if(this.cards[this.currCardClicked].getRect().Right>=this.discardZone.getRect().Left) {
                            cardPlayedDispLbl.Text = 
                                this.cards[this.currCardClicked].currCard.value.ToString();
                        } else {
                            cardPlayedDispLbl.Text = "Dragging...";
                    }
                }
                
            }

        } //end event



        /// <summary>
        /// Timer Ticking Event - Draw Control -------------------------
        /// </summary>
        /// <param name="sender">object - Form Control Sender</param>
        /// <param name="e">MouseEventArgs - Form Mouse Event</param>
        private void frameRateTimer_Tick(object sender, EventArgs e) {
            //Update Play Area View
            this.Invalidate();

        } //end event


        /// <summary>
        /// Timer Ticking Event - Update Control -------------------------
        /// </summary>
        /// <param name="sender">object - Form Control Sender</param>
        /// <param name="e">MouseEventArgs - Form Mouse Event</param>
        private void functionalityTimer_Tick(object sender, EventArgs e) {

            if(this.gameOver==false) {
                //Update All HUD Info
                levelDisplayLabel.Text = this.game.player.Level.ToString();
                directionDisplayLabel.Text = this.game.Direction.ToString();
                turnDisplayLabel.Text = this.game.GetTurn.ToString();
            
                //Stop User From Playing Out Of Turn
                if(this.game.GetWhosTurn!=0) {
                    this.RemoveCardDragging();    
                    this.Paint -= this.DrawButton_Paint;

                } else if(this.game.GetWhosTurn==0) {
                
                    //Ensure There are moves
                    if(this.game.player.CheckHandForMoves(this.game.Direction, this.game.LookTopDiscard())) {
                        this.AddCardDragging();
                        this.Paint -= this.MessageBox_Paint;
                        this.Paint -= this.NextButton_Paint;
                    
                    //If No Moves, Inform User
                    } else {
                        this.Paint -= this.NextButton_Paint;
                        this.messageBox.SetMessage(userCantGo + " " + userDraw);
                        this.Paint += this.DrawButton_Paint;
                    }
               
                }

                //Game Win Condition
                if(this.game.player.hand.Length<=0) {
                    this.WipeScreen();
                    this.gameOver = true;
                    this.messageBox.SetMessage(gameWon);
                }

                //AI Win Conditions
                if(this.game.aiPlayers.CheckAIWinner()) {
                    this.WipeScreen();
                    this.gameOver = true;
                    this.messageBox.SetMessage(gameLost);
                }

            //Game Over
            } else {
                this.Paint += this.MessageBox_Paint;
                this.Invalidate();
            }

        }

        private void aiTimer_Tick(object sender, EventArgs e) {
                Card tempCard = this.game.TakeAITurn();

                if(tempCard!=null)
                    this.PlayAICard(tempCard);
                else 
                    this.messageBox.SetMessage(aiCantGo);
                    this.Paint += this.MessageBox_Paint;
                    this.Paint += this.NextButton_Paint;
        } //end event


        // =====================================
                

    } //EOC

} //EON
