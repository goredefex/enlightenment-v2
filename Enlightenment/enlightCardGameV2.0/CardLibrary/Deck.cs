﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardLibrary {
    public class Deck : DeckModel {

        //Deck Controls
        private const int cardsPerDeck = 52;
        private int deckSize = 0;
        private int totalCards = 0;
        private Random r = new Random((int) DateTime.Now.Ticks & 0x0000FFFF);
        
        //Deck Of Cards
        public List<Card> cards;
        

        // =====================================
        //Constructs ---------------------------
        // =====================================

        /// <summary>
        /// Standard Size Deck Is Default
        /// [52 Cards]
        /// </summary>
        public Deck() {
            this.deckSize = 1;
            this.totalCards = this.FindStandardTotal();
            this.cards = this.BuildCurrDeck();

        } //end constructor


        /// <summary>
        /// How Many Standard Size 
        /// Decks To Create A Size Of 
        /// [Standard Size = 52]
        /// </summary>
        /// <param name="size">int</param>
        public Deck(int size) {
            if(size > 1) {
                this.deckSize = size;
                this.totalCards = this.FindStandardTotal();
                this.cards = this.BuildCurrDeck();

            } else { //<-- Catches Negatives & 0
                this.deckSize = 1;
                this.totalCards = this.FindStandardTotal();
                this.cards = this.BuildCurrDeck();
            
            }
        
        } //end constructor


        /// <summary>
        /// Builds A Deck Of Non-Standard
        /// Size by cardCount and Insert
        /// All New Cards To Use As New Deck
        /// </summary>
        public Deck(int cardCount, List<Card> newCardsForDeck) {
            this.totalCards = cardCount;
            this.cards = new List<Card>(this.totalCards);
            this.cards.AddRange(newCardsForDeck);
        
        } //end constructor

        // ==========================================



        // =====================================
        //Functions ----------------------------
        // =====================================


        /// <summary>
        /// Finds Card Amount For Game
        /// </summary>
        /// <returns>Integer Card Count</returns>
        private int FindStandardTotal() { return this.deckSize * cardsPerDeck; } //end function


        public int GetCardCount() {
            return this.totalCards;
        
        } //end function


        /// <summary>
        /// Creates A Pre-Seeded Random Number
        /// </summary>
        /// <param name="newNum">Int - Max Random Number</param>
        /// <param name="hasZero">Bool - Can Output Include 0 Base or Begin at 1</param>
        /// <returns>Integer Random Number</returns>
        private int generateRand(int newNum, bool hasZero) {
            int rand = this.r.Next(newNum);
		
	        if(rand==0 && hasZero==false) {
		        rand++;
	        }
		
	        return rand;

        } //end function

        
        /// <summary>
        /// Takes into account the current
        /// selected size of the deck and builds
        /// card list.
        /// </summary>
        /// <returns>New List Of Cards In Ascending Value Order</returns>
        private List<Card> BuildCurrDeck() {
            List<Card> newDeck = new List<Card>();
            for(int numDecks=0; numDecks<this.deckSize; numDecks++) { //<--How Many Decks
                for(int cardVals=(int)cardValue.joker; cardVals<=(int)cardValue.king; cardVals++) { //<--Add Card Value
                    for(int suits=(int)cardSuit.spade; suits<=(int)cardSuit.heart; suits++) { //<--Add Card Suits
                        newDeck.Add(new Card(cardVals, suits)); //<--Insert Card To List
                    }
                }
            }
        
            return newDeck;

        } //end function
        

        /// <summary>
        /// Shuffles Current Deck List
        /// </summary>
        /// <param name="shuffleXTimes">Int - How many times to shuffle</param>
        public void ShuffleDeck(int shuffleXTimes) {
            int stepCounter = 0;

            for(int shuffleCounter=0; shuffleCounter<shuffleXTimes; shuffleCounter++) {
                while(stepCounter < this.cards.Count) {
                    int swapPos = this.generateRand(this.cards.Count, true);
                    this.cards[stepCounter] = this.cards[swapPos];
                    stepCounter++;

                }
            }
        
        } //end function


        /// <summary>
        /// Sets All Cards In Deck Of One Suit
        /// To Be Now Considered Trump.
        /// </summary>
        /// <param name="currTrump">Deck.cardSuit - Suit To Make Trump</param>
        public void setTrumpSuit(Deck.cardSuit currTrump) {
            int stepCounter = 0;
            while(stepCounter < this.cards.Count) {
                if(this.cards[stepCounter].suit == (int)currTrump) {
                    this.cards[stepCounter].isTrump = true;
                }
                stepCounter++;
            }
        
        } //end function


        /// <summary>
        /// Finds List Location Number of
        /// first card matching value and suit
        /// </summary>
        /// <param name="val">Deck.cardValue</param>
        /// <param name="suit">Deck.cardSuit</param>
        /// <returns>Integer - Position of first card in deck of entered specs</returns>
        public int FindACardLocation(Deck.cardValue val, Deck.cardSuit suit) {
            int stepCounter = 0;
            while(stepCounter < this.cards.Count) {
                if(this.cards[stepCounter].value == (int)val && this.cards[stepCounter].suit == (int)suit) {
                    break;
                }
                stepCounter++;
            }
        
            return stepCounter;

        } //end function


    } //EOC

} //EON