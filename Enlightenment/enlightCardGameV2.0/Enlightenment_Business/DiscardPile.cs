﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CardLibrary;

namespace Enlightenment_Business {
    public class DiscardPile {

        //[CONTAINER CLASS]

        //Size Controls
        private const int miniSize = 4;

        //Member Vars
        private List<Card> cards = new List<Card>();
        private List<Card> miniCards = new List<Card>(miniSize);

        // =====================================
        //Fields -------------------------------
        // =====================================
        

        /// <summary>
        /// Field Linked To MiniCards
        /// </summary>
        public List<Card> MiniCards { get { return miniCards; } 
                                      set { this.miniCards=value; } }


        /// <summary>
        /// Returns Cards In Pile
        /// </summary>
        public List<Card> GetCards { get { return this.cards; } }

        
        /// <summary>
        /// Field Linked To The Size Of The MiniCards
        /// </summary>
        public int MiniSize { get { return miniSize; } }


        // =====================================
        //Functions ----------------------------
        // =====================================
        

        /// <summary>
        /// Empties Pile, Usually For Shuffle 
        /// Back In Functions
        /// </summary>
        public void WipeDeck() { this.cards.Clear(); } //end function


        /// <summary>
        /// Resturns The Current Size Of The Discard Pile
        /// </summary>
        /// <returns></returns>
        public int DiscardSize() { return this.cards.Count; } //end function


        /// <summary>
        /// Enters The Card Given Into
        /// Both The Discard Pile As Well
        /// As The Mini Card List
        /// </summary>
        /// <param name="currCard"></param>
        public void Discard(Card currCard) {
            this.cards.Add(currCard);
            this.miniCards.Add(currCard);

            //If Too Big, Dump, Reset
            if(this.miniCards.Count>=5) {
                List<Card> temp = new List<Card>(4);
                temp.Add(this.miniCards[this.miniCards.Count-1]);
                temp.Add(this.miniCards[this.miniCards.Count-2]);
                temp.Add(this.miniCards[this.miniCards.Count-3]);
                temp.Add(this.miniCards[this.miniCards.Count-4]);
                this.miniCards.Clear();
                this.miniCards = temp;
                temp = null;

            }
                    
        } //end function
                

        /// <summary>
        /// Returns The Top Of The Pile
        /// Without Removing It
        /// </summary>
        /// <returns></returns>
        public Card Top() { 
            if(this.cards.Count>0) {
                if(this.cards[this.cards.Count-1]==null)
                    return null;
                else
                    return this.cards[this.cards.Count-1];

            } else {
                return null;

            }

        } //end function


        /// <summary>
        /// Returns The Card In The Pile
        /// Of Index Given
        /// </summary>
        /// <returns></returns>
        public Card GetDiscardByNum(int cardNum) { 
            if(this.cards[cardNum]!=null) {
                return this.cards[cardNum];

            } else { return null; }

        } //end function


    } //EOC

} //EON
