﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CardLibrary;

namespace Enlightenment_Business {
    public class Player : Rules {
    
        private int level = 1;
        private int cardsPerHand = 0;
        public Card[] hand;
               

        // =====================================
        //Constructs ---------------------------
        // =====================================

        //Takes A Size Of Hand
        public Player(int handSize) {
            this.hand = new Card[handSize];
            this.cardsPerHand = handSize;
        
        } //end construct

        // =====================================


        
        // =====================================
        //Fields -------------------------------
        // =====================================
        
        /// <summary>
        /// Gets The Current Level
        /// </summary>
        /// <returns>Integer - Returns Current Level</returns>
        public int Level { get {return this.level; }
                           set { this.level=value; } } //end function



        /// <summary>
        /// Removes And Plays A Card
        /// From The Users Hand
        /// </summary>
        /// <param name="cardSpot">Integer - Card To Remove</param>
        public void PlayCardFromUsersHand(int cardSpot) {
            this.hand[cardSpot] = null;
        
        } //end function



        /// <summary>
        /// Take A Card From  Hand.
        /// Currently - If No Room In Hand,
        /// Card Is Not Entered.
        /// CHANGE - Sprint.Iter4
        /// </summary>
        /// <param name="currCard"></param>
        public void PutCardInHand(Card currCard) {
            for(int i=0; i<cardsPerHand; i++) {
                if(this.hand[i]==null) {
                    this.hand[i]=currCard;
                    i=cardsPerHand;
                }

                //Add this in with the change of arrays
                //to hand lists and .lengths to .counts
                //if(i==(cardsPerHand-1) && this.player.hand[i]!=null)
                //    //this.player.hand.add(currCard);
                //    this.discards.Add(currCard);
            }

        } //end function



        /// <summary>
        /// Search Cards In Hand For The
        /// Ability To Lay A Card Down, Given 
        /// The Card Sent In
        /// </summary>
        /// <param name="currDir">direction - Direction of the gameplay</param>
        /// <param name="topPile">Card - Card sitting on the discard pile</param>
        /// <returns>True or False - IF there are any moves for the user</returns>
        public bool CheckHandForMoves(direction currDir, Card topPile) {
        
            bool moves = false;

            switch(currDir) {
                case direction.none: //<--No Direction
                    moves = true;
                    break;
                case direction.up: //<--Up Direction
                    for(int i=0; i<this.hand.Length; i++) {
                        if(this.hand[i]!=null && this.hand[i].value>topPile.value) {
                            moves=true;
                            i=this.hand.Length;
                        }
                    }
                    break;
                case direction.down: //<--Up Direction
                    for(int i=0; i<this.hand.Length; i++) {
                        if(this.hand[i]!=null && this.hand[i].value<topPile.value) {
                            moves=true;
                            i=this.hand.Length;
                        }
                    }
                    break;
            } 
           
            return moves;
        
        } //end function

        // =====================================

    
    } //EOC

} //EON
