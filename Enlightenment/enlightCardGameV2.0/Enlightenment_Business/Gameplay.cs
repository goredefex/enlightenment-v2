﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CardLibrary;

namespace Enlightenment_Business {
    public class Gameplay : Rules {
                
        //Game Rules/Sizing
        private const int gameDecksUsed = 2;
        private const int cardsPerHand = 9;
        private const int user = 0;
        
        //Non-Accessible
        private int numberPlayers = 4;
        private int turn = 1;
        private int whosTurn = 0;
        private bool askedMoves = false;
        
        //Game Accessible Objects
        public Deck deck;
        public Deck luckDeck;
        public DiscardPile discards = new DiscardPile();
        public Player player = new Player(cardsPerHand);
        public AI aiPlayers;
        
        //Properties
        public direction currentDirection = direction.none;


        // =====================================
        //Constructs ---------------------------
        // =====================================

        /// <summary>
        /// Default Game Which Has 4 AI Players
        /// </summary>
        public Gameplay() { //<-- Default
            this.aiPlayers = new AI(numberPlayers, cardsPerHand);
            this.deck = new Deck(gameDecksUsed); //<--Make 2 Decks
            this.SeperateLuckPlayDecks(); //<--Seperate in 2
            this.luckDeck.ShuffleDeck(20); //<--Shuffle
            this.deck.ShuffleDeck(20); //<--Shuffle    

            this.DealAHand(); //Opening Hand
        
        } //end construct

       
        /// <summary>
        /// Game Takes (x) AI Players Passed In
        /// </summary>
        /// <param name="numPlayers"></param>
        public Gameplay(int numPlayers) { //<-- Default
            this.numberPlayers = numPlayers;
            this.aiPlayers = new AI(numberPlayers, cardsPerHand);
            this.deck = new Deck(gameDecksUsed); //<--Make 2 Decks
            this.SeperateLuckPlayDecks(); //<--Seperate in 2
            this.luckDeck.ShuffleDeck(10); //<--Shuffle
            this.deck.ShuffleDeck(10); //<--Shuffle    
 
            this.DealAHand(); //Opening Hand
                    
        } //end construct

        // =====================================


        // =====================================
        //Fields -------------------------------
        // =====================================


        /// <summary>
        /// Gets or Sets Weither A Move
        /// Was Found For The User Or Not
        /// </summary>
        public bool UserAskedMoves { get { return this.askedMoves; } 
                                     set { this.askedMoves=value; } 
                                   }

        /// <summary>
        /// Get or Set The Direction
        /// </summary>
        public direction Direction { get { return this.currentDirection; } 
                                     set { this.currentDirection=value; } 
                                   }

        /// <summary>
        /// Finds How Many Cards Are Per Hand
        /// </summary>
        /// <returns>Integer - Number of Cards Per Hand</returns>
        public int GetNumberOfCards { get { return cardsPerHand; } } //end function


        /// <summary>
        /// Finds How Many Players Are Playing
        /// </summary>
        /// <returns></returns>
        public int GetNumberOfPlayers { get { return numberPlayers; } } //end function


        /// <summary>
        /// Gets the current turn
        /// </summary>
        /// <returns>Integer - Returns the turn counter</returns>
        public int GetTurn { get { return this.turn; } } //end function
        

        /// <summary>
        /// Gets the player whos turn it is
        /// </summary>
        /// <returns>Integer - Returns the whosTurn variable</returns>
        public int GetWhosTurn { get { return this.whosTurn; } } //end function


        /// <summary>
        /// Gets the key number for the user
        /// </summary>
        public int GetUserNumber { get { return user; } } //end function


        // =====================================
        //Functions ----------------------------
        // =====================================
        
        /// <summary>
        /// Deals The Top Card On The Deck
        /// </summary>
        /// <returns>Card Object</returns>
        public Card TopCard() { 
            if(this.deck.cards.Count<=1) {
                this.ShuffleInDiscardPile();
            }
            Card tempCard = this.deck.cards[0];
            this.deck.cards.RemoveAt(0);

            return tempCard;
        
        } //end function


        /// <summary>
        /// Views Top Card On Discard Pile
        /// </summary>
        /// <returns>Card - Discard Card</returns>
        public Card LookTopDiscard() { return this.discards.Top(); } //end function


        /// <summary>
        /// Advances The Turn Integer Counter
        /// By One Turn
        /// </summary>
        public void AdvanceTurnCounter() { this.turn++; } //end function


        /// <summary>
        /// Changes The whosTurn Variable. 
        /// If It Is The Last Persons Turn It
        /// Resets To User-0
        /// </summary>
        public void NextPersonsTurn() {
            if(this.whosTurn<this.numberPlayers-1)
                this.whosTurn++;
            else
                this.whosTurn=0;
        
        } //end function


        /// <summary>
        /// Seperates Deck Into Two Decks
        /// A Luck Deck and the Play Deck
        /// </summary>
        public void SeperateLuckPlayDecks() {
            int counter = 0;
            List<Card> luckCardsFound = new List<Card>(); //<--Container

            //As Long As There Are Cards, Check For Luck Cards
            while(counter<this.deck.cards.Count) {
                if(this.deck.cards[counter].value == (int)Deck.cardValue.king 
                    || this.deck.cards[counter].value == (int)Deck.cardValue.queen
                    || this.deck.cards[counter].value == (int)Deck.cardValue.ace 
                    || this.deck.cards[counter].value == (int)Deck.cardValue.Ace) {
                
                    luckCardsFound.Add(this.deck.cards[counter]);
                    this.deck.cards.RemoveAt(counter);
                    counter--;

                } else if(this.deck.cards[counter].value == (int)Deck.cardValue.joker) {
                    this.deck.cards.RemoveAt(counter);
                    counter--;
                }
                counter++;
            }

            //Make Luck Deck
            this.luckDeck = new Deck(luckCardsFound.Count, luckCardsFound);

        } //end function


        /// <summary>
        /// Deals A Hand to all players.
        /// If There Are Not Enough Cards Left
        /// In The Deck, ShuffleInDiscardPile()
        /// Is Used.
        /// </summary>
        public void DealAHand() {
            if(this.deck.cards.Count<=(cardsPerHand*numberPlayers)) {
                this.ShuffleInDiscardPile();
            } else {
                //If There are cards, deal a hand
                for(int card2Deal=0; card2Deal<cardsPerHand; card2Deal++) {
                    this.player.hand[card2Deal] = this.TopCard();
                    for(int players=user; players<(this.numberPlayers-1); players++) {
                        this.aiPlayers.AIHands[players,card2Deal] = this.TopCard();
                    }   
                }
            }

        } //end function


        /// <summary>
        /// Takes The Discard Pile and
        /// Returns The Cards Back To The
        /// Deck And Shuffles 10 Times
        /// </summary>
        public void ShuffleInDiscardPile() {
            //Loop Till Count-4 Leaving Mini Views
            for(int i=0; i<this.discards.DiscardSize()-4; i++) {
                this.deck.cards.Add(this.discards.GetCards[i]);
            }
            this.discards.WipeDeck();
            this.deck.ShuffleDeck(10);
            
        } //end function


        /// <summary>
        /// Takes A The Card Being Played
        /// And The Card On The Top Of The
        /// Discard Pile And Chooses Direction
        /// </summary>
        public void SetDirectionOfPile(Card newCard) {
            if(this.Direction==direction.none && this.LookTopDiscard()!=null) {
                if(newCard.value>this.LookTopDiscard().value) {
                    this.Direction = direction.up;
                } else if(newCard.value<this.LookTopDiscard().value) {
                    this.Direction = direction.down;
                }
            }
        
        } //end function



        /// <summary>
        /// Takes Given Card & Checks
        /// If Current Play Is A Legal Play
        /// </summary>
        /// <param name="currCard">Card - Card In Question</param>
        /// <returns>True or False - Legal</returns>
        public bool CheckIfCardLegal(Card currCard) {
            
            bool legal = false;

            //Min 1 Card In Discard
            if(this.discards.GetCards.Count!=0) {
                //If Not The Same Value
                if(currCard.value!=this.LookTopDiscard().value) {    
                    switch(this.Direction) {
                        case direction.none: //<--No Direction
                            legal = true;
                            break;
                        case direction.up: //<--Up Direction
                            if(currCard!=null && currCard.value>this.LookTopDiscard().value) 
                                legal=true;
                            break;
                        case direction.down: //<--Up Direction
                            if(currCard!=null && currCard.value<this.LookTopDiscard().value) 
                                legal=true;
                            break;
                    }
                }

            //Nothing In Discard Pile Yet
            } else { legal = true; }
           
            return legal;
        
        } //end function


        /// <summary>
        /// Duties Required Each User Turn
        /// </summary>
        public void TakeUserTurn() {
            if(this.LookTopDiscard()!=null) {
                //Change Direction If Upp/Low Bound
                if(this.LookTopDiscard().value==this.upperBoundValue) {
                    this.Direction = direction.down;
                } else if(this.LookTopDiscard().value==this.lowerBoundValue) {
                    this.Direction = direction.up;
                }
            }
        
        } //end function
        


        /// <summary>
        /// Duties Required To Have AI
        /// Take A Turn And Choose A
        /// Card Based On Game Rules
        /// </summary>
        /// <param name="topPile">Card - Top Of Discard Pile</param>
        /// <returns>Card - AI Card Chosen From AI Hand</returns>
        public Card TakeAITurn() {

            Card currCard = null; //<--Temp Card Container
            //AI Makes Choice
            int cardNumChoice = this.aiPlayers.AIDecideLogic(this.Direction, (this.whosTurn-1), this.LookTopDiscard());

            //If AI Chose Something
            if(cardNumChoice>=0) {
                //If Card AI Chose Is Allowed
                if(this.CheckIfCardLegal(this.aiPlayers.AIHands[this.whosTurn-1, cardNumChoice])) {
                    //Ensure Direction Choice When None
                    this.SetDirectionOfPile(this.aiPlayers.AIHands[this.whosTurn-1, cardNumChoice]);
                    
                    //Change Direction If Upp/Low Bound
                    if(this.aiPlayers.AIHands[this.whosTurn-1, cardNumChoice].value==this.upperBoundValue) {
                        this.Direction = direction.down;
                    } else if(this.aiPlayers.AIHands[this.whosTurn-1, cardNumChoice].value==this.lowerBoundValue) {
                        this.Direction = direction.up;
                    } 
                    //Grab Card Before Removal
                    if(this.aiPlayers.AIHands[this.whosTurn-1, cardNumChoice]!=null)
                        currCard = this.aiPlayers.AIHands[this.whosTurn-1, cardNumChoice];

                    //Remove Card From Hand
                    this.aiPlayers.AIHands[this.whosTurn-1, cardNumChoice] = null;
                }

            /*Draw Card*/     
            } else { this.aiPlayers.PutCardInAIHand(this.TopCard(), this.whosTurn-1); }
                       
            return currCard;
        
        } //end function
                

        // =====================================



    } //EOC
    
} //EON
