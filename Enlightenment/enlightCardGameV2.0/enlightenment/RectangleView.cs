﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Threading.Tasks;

namespace Enlightenment_Presentation {
    public abstract class RectangleView {

        //Painting Objects
        private Rectangle currRect;
        private SolidBrush brush = new SolidBrush(Color.White);
        private SolidBrush wordBrush = new SolidBrush(Color.Black);

        //Member Vars
        private int x = 0;
        private int y = 0;
        private int width = 0;
        private int height = 0;

        //Rectangle Array
        public Rectangle[] recArray;
        


        // =====================================
        //Getters ------------------------------
        // =====================================

        /// <summary>
        /// Gets Member Width
        /// </summary>
        /// <returns>Integer - Width</returns>
        public int getWidth() { return this.width; } //end function

        /// <summary>
        /// Gets Member Height
        /// </summary>
        /// <returns>Integer - Height</returns>
        public int getHeight() { return this.height; } //end function

        /// <summary>
        /// Gets Member X Coordinate
        /// </summary>
        /// <returns>Integer - X Coordinate</returns>
        public int getX() { return this.x; } //end function
        
        /// <summary>
        /// Gets Member Y Coordinate
        /// </summary>
        /// <returns>Integer - Y Coordinate</returns>
        public int getY() { return this.y; } //end function

        /// <summary>
        /// Gets Member Rectangle
        /// </summary>
        /// <returns>Rectangle</returns>
        public Rectangle getRect() { return this.currRect; } //end function

        // =====================================



        // =====================================
        //Setters ------------------------------
        // =====================================

        /// <summary>
        /// Sets Member Width
        /// </summary>
        /// <param name="newWidth">Integer - Sets Width</param>
        public void setWidth(int newWidth) { this.width = newWidth; } //end function

        /// <summary>
        /// Sets Member Height
        /// </summary>
        /// <param name="newWidth">Integer - Sets Height</param>
        public void setHeight(int newHeight) { this.height = newHeight; } //end function

        /// <summary>
        /// Sets Member X Coordinate
        /// </summary>
        /// <param name="newWidth">Integer - Sets X Coordinate</param>
        public void setX(int newX) { this.x = newX; } //end function

        /// <summary>
        /// Sets Member Y Coordinate
        /// </summary>
        /// <param name="newWidth">Integer - Sets Y Coordinate</param>
        public void setY(int newY) { this.y = newY; } //end function

        /// <summary>
        /// Sets Member Rectangle
        /// </summary>
        /// <param name="newWidth">Rectangle Obj</param>
        public void setRect(Rectangle r) { this.currRect =  r; } //end function

        // =====================================

        

        // =====================================
        //Functions ----------------------------
        // =====================================

        /// <summary>
        /// Draws & Re-creates Rectangle Object
        /// </summary>
        /// <param name="e">PaintEventArgs - Form Paint Event</param>
        public void MakeRectangle(System.Windows.Forms.PaintEventArgs e) {
            this.currRect = new Rectangle(this.x, this.y, this.width, this.height);
            e.Graphics.FillRectangle(this.brush, this.currRect);
        
        } //end function


        /// <summary>
        /// Draws & Re-creates Rectangle Image
        /// </summary>
        /// <param name="e">PaintEventArgs - Form Paint Event</param>
        /// <param name="img">Image - Image To Draw</param>
        public void MakeRectangleImage(System.Windows.Forms.PaintEventArgs e, Image img) {
            this.currRect = new Rectangle(this.x, this.y, this.width, this.height);
            e.Graphics.DrawImage(img, this.currRect);
        
        } //end function
        

        /// <summary>
        /// Draws Image On Rectangle
        /// </summary>
        /// <param name="e">PaintEventArgs - Form Paint Event</param>
        public void MakeImageOnRect(System.Windows.Forms.PaintEventArgs e, 
                                        Image img, int newX, int newY, int newWidth, int newHeight) {
            e.Graphics.DrawImage(img, newX, newY, newWidth, newHeight);
        
        } //end function
        

        /// <summary>
        /// Draws & Recreates Rectangle With Image As Rect Size
        /// </summary>
        /// <param name="e">PaintEventArgs - Form Paint Event</param>
        /// <param name="img">Bitmap - Image To Display</param>
        public void MakeRectangleBmp(System.Windows.Forms.PaintEventArgs e, Bitmap img) {
            this.currRect = new Rectangle(this.x, this.y, this.width, this.height);
            e.Graphics.DrawImage(img, this.currRect);
        
        } //end function


        /// <summary>
        /// Draws String On Rectangle
        /// </summary>
        /// <param name="e">PaintEventArgs - Form Paint Event</param>
        /// <param name="font">Font Obj - Font Type</param>
        /// <param name="text">string - String To View</param>
        /// <param name="newX">Integer - X Coordinate</param>
        /// <param name="newY">Integer - Y Coordinate</param>
        public void MakeString(System.Windows.Forms.PaintEventArgs e, Font font, String text, int newX, int newY) {
            e.Graphics.DrawString(text, font, this.wordBrush, new Point(newX, newY));
                    
        } //end function


        /// <summary>
        /// Look in rectangle bounds to see if
        /// cursors entered is in rectangle view
        /// </summary>
        /// <param name="newCursor">Point - Current Cursor Location</param>
        /// <returns>True or False - Is/Isn't In Rectangle</returns>
        public bool CheckCursorInRect(Point newCursor) {
            if(this.currRect.Contains(newCursor)) {
                return true;
            } else {
                return false;
            }
        
        } //end function


        /// <summary>
        /// Change Color Of Rectangle
        /// </summary>
        /// <param name="color">Color Obj</param>
        public void ChangeRectColor(Color color) {
            this.brush.Color = color; //= new SolidBrush(color);
        
        } //end function


        /// <summary>
        /// change Color Of Wording
        /// </summary>
        /// <param name="color">Color Obj</param>
        public void ChangeWordColor(Color color) {
            this.wordBrush = new SolidBrush(color);
        
        } //end function


        // =====================================


    } //EOC
    
} //EON
