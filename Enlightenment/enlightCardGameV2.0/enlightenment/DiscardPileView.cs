﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Threading.Tasks;

namespace Enlightenment_Presentation {
    public class DiscardPileView : RectangleView {

        //Const Size Vars
        private const int discardX = 395;
        private const int discardY = 140;
        private const int discardWidth = 300;
        private const int discardHeight = 300;
        private const int pileX = 505;
        private const int pileY = 230;

        // =====================================
        //Constructs ---------------------------
        // =====================================

        public DiscardPileView() { //<-- Default
            this.setX(discardX);
            this.setY(discardY);
            this.setWidth(discardWidth);
            this.setHeight(discardHeight);
            this.ChangeRectColor(Color.DarkOliveGreen);
        
        } //end construct

        // =====================================


        // =====================================
        //Getters ------------------------------
        // =====================================

        public int GetPileX() { return pileX; } //end function

        public int GetPileY() { return pileY; } //end function


        // =====================================
        //Functions ----------------------------
        // =====================================

        /// <summary>
        /// Draws Discard Pile At Member X,Y
        /// Vars Set Previously
        /// </summary>
        /// <param name="e">PaintEventArgs - Form Paint Event</param>
        public void DrawDiscardPile(System.Windows.Forms.PaintEventArgs e) {
            this.MakeRectangle(e);
        
        } //end function


        // =====================================

    } //EOC

} //EON
