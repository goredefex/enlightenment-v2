﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using CardLibrary;

namespace Enlightenment_Presentation {
    public class MiniCardView : RectangleView {


        //Mini Const Size Vars
        private const int numberOfMinis = 4;
                       
        //Mini Sizing
        private const int miniWidth = 55;
        private const int miniheight = 70;
        private const int spacingSize = 6;
        private const int cardNumWidth = 14;
        private const int cardNumHeight = 20;
        private const int cardNumTenWidth = 20;
        private const int cardSuitWidth = 12;
        private const int cardSuitHeight = 15;
        private const int decalEdgeSpacing = 3;

        //Mini Objects
        private Card currCard = null;

        //Decal Objects
        private Font font = new Font("Microsoft Sans Serif", 10, FontStyle.Bold);
        

        // =====================================
        //Constructs ---------------------------
        // =====================================

        public MiniCardView() { //<-- Default
            this.setX(0);
            this.setY(0);
            this.setWidth(miniWidth);
            this.setHeight(miniheight);
            this.ChangeRectColor(Color.ForestGreen);
        
        } //end construct

        //Takes X,Y Coordinates
        public MiniCardView(int newX, int newY) {
            this.setX(newX);
            this.setY(newY);
            this.setWidth(miniWidth);
            this.setHeight(miniheight);
            this.ChangeRectColor(Color.ForestGreen);
        
        } //end construct

        //Takes Card, X,Y Coordinates
        public MiniCardView(Card newCard, int newX, int newY) {
            this.currCard = newCard;
            this.setX(newX);
            this.setY(newY);
            this.setWidth(miniWidth);
            this.setHeight(miniheight);
            this.ChangeRectColor(Color.White);
        
        } //end construct

        // =====================================


        // =====================================
        //Fields -------------------------------
        // =====================================
        
        /// <summary>
        /// Gets The Width of The Hand Holder
        /// </summary>
        public int GetMiniWidth { get { return miniWidth; } } //end field

        /// <summary>
        /// Gets The Height of The Hand Holder
        /// </summary>
        public int GetMiniHeight { get {  return miniheight; } } //end field

        /// <summary>
        /// Gets The Padding of The Hand Holder Sidings
        /// </summary>
        public int GetSpacingSize { get {  return spacingSize; } } //end field
        
        /// <summary>
        /// Gets The Number of Mini Cards
        /// </summary>
        public int GetNumMinis { get { return numberOfMinis; } } //end field


        // =====================================
        //Functions ----------------------------
        // =====================================

        /// <summary>
        /// Draws Given Card Settings
        /// Pre Set When Built or Manually
        /// Added
        /// </summary>
        /// <param name="e">PaintEventArgs - Form Paint Event</param>
        public void DrawMiniCard(System.Windows.Forms.PaintEventArgs e) {

            if(this.currCard!=null) {
                //If A Non-Face Card
                if(this.currCard.value<(int)Deck.cardValue.jack || this.currCard.value==(int)Deck.cardValue.ace) {
                    this.MakeRectangle(e); //<-- Reprint New Rect
                    this.MakeAndPlaceCardDecals(e);

                //If Face Card
                } else {
                    this.SetAndPlaceFacing(e);
                
                }

            } else {
                this.MakeRectangle(e);
            
            }
        
        } //end function
        
        // =====================================



        // =====================================
        //Private ------------------------------
        // =====================================
  
        /// <summary>
        /// Creates All Image/Number Decals
        /// To Represent Suit/Value
        /// </summary>
        /// <param name="e">PaintEventArgs - Form Paint Event</param>
        public void MakeAndPlaceCardDecals(System.Windows.Forms.PaintEventArgs e) { 
            if(this.currCard.value<(int)Deck.cardValue.jack) {

                //Top Num Label
                this.MakeString(e, this.font, this.currCard.value.ToString(), 
                    this.getX(), this.getY());

                //Bottom Num Label
                if(this.currCard.value!=(int)Deck.cardValue.ten)
                    this.MakeString(e, this.font, this.currCard.value.ToString(), 
                        (this.getRect().Right-cardNumWidth), (this.getRect().Bottom-cardNumHeight));

                if(this.currCard.value==(int)Deck.cardValue.ten)
                    this.MakeString(e, this.font, this.currCard.value.ToString(), 
                        (this.getRect().Right-cardNumTenWidth), (this.getRect().Bottom-cardNumHeight));

                
                //Top Suit Pic
                this.AddSuitPic(e,
                    ((this.getRect().Right-cardSuitWidth)-decalEdgeSpacing), this.getY()+decalEdgeSpacing);

                //Bottom Suit Pic
                this.AddSuitPic(e, 
                    this.getX()+decalEdgeSpacing, ((this.getRect().Bottom-cardSuitHeight)-decalEdgeSpacing));

            }
        
        } //end function 


        /// <summary>
        /// Adds In Any Suit Image Decals
        /// To Draw
        /// </summary>
        /// <param name="e">PaintEventArgs - Form Paint Event</param>
        /// <param name="newX">Integer - X Coordinate</param>
        /// <param name="newY">Integer - Y Coordinate</param>
        private void AddSuitPic(System.Windows.Forms.PaintEventArgs e, int newX, int newY) {
            if(this.currCard.suit == (int)Deck.cardSuit.club) {
                Image image = global::Enlightenment_Presentation.Properties.Resources.whiteClub;
                this.MakeImageOnRect(e, image, newX, newY, cardSuitWidth, cardSuitHeight);
            } else if(this.currCard.suit == (int)Deck.cardSuit.diamond) {
                Image image = global::Enlightenment_Presentation.Properties.Resources.whiteDiamond;
                this.MakeImageOnRect(e, image, newX, newY, cardSuitWidth, cardSuitHeight);
            } else if(this.currCard.suit == (int)Deck.cardSuit.heart) {
                Image image = global::Enlightenment_Presentation.Properties.Resources.whiteHeart;
                this.MakeImageOnRect(e, image, newX, newY, cardSuitWidth, cardSuitHeight);
            } else if(this.currCard.suit == (int)Deck.cardSuit.spade) {
                Image image = global::Enlightenment_Presentation.Properties.Resources.whiteSpade;
                this.MakeImageOnRect(e, image, newX, newY, cardSuitWidth, cardSuitHeight);
            }
        } //end function


        private void SetAndPlaceFacing(System.Windows.Forms.PaintEventArgs e) {

            switch (this.currCard.value) {
                //Aces Facings --------------------------------------
                case (int)Deck.cardValue.ace:
                    if(this.currCard.suit==(int)Deck.cardSuit.club) {
                        Image image = global::Enlightenment_Presentation.Properties.Resources.aceClubs;
                        this.MakeRectangleImage(e, image);
                    } else if(this.currCard.suit==(int)Deck.cardSuit.diamond) {
                        Image image = global::Enlightenment_Presentation.Properties.Resources.aceDiamonds;
                        this.MakeRectangleImage(e, image);
                    } else if(this.currCard.suit==(int)Deck.cardSuit.heart) {
                        Image image = global::Enlightenment_Presentation.Properties.Resources.aceHearts;
                        this.MakeRectangleImage(e, image);
                    } else if(this.currCard.suit==(int)Deck.cardSuit.spade) {
                        Image image = global::Enlightenment_Presentation.Properties.Resources.aceSpades;
                        this.MakeRectangleImage(e, image);
                    }
                    break;

                //Aces Facings --------------------------------------
                case (int)Deck.cardValue.Ace:
                    if(this.currCard.suit==(int)Deck.cardSuit.club) {
                        Image image = global::Enlightenment_Presentation.Properties.Resources.aceClubs;
                        this.MakeRectangleImage(e, image);
                    } else if(this.currCard.suit==(int)Deck.cardSuit.diamond) {
                        Image image = global::Enlightenment_Presentation.Properties.Resources.aceDiamonds;
                        this.MakeRectangleImage(e, image);
                    } else if(this.currCard.suit==(int)Deck.cardSuit.heart) {
                        Image image = global::Enlightenment_Presentation.Properties.Resources.aceHearts;
                        this.MakeRectangleImage(e, image);
                    } else if(this.currCard.suit==(int)Deck.cardSuit.spade) {
                        Image image = global::Enlightenment_Presentation.Properties.Resources.aceSpades;
                        this.MakeRectangleImage(e, image);
                    }
                    break;

                //Jacks Facings -------------------------------------
                case (int)Deck.cardValue.jack:
                    if(this.currCard.suit==(int)Deck.cardSuit.club) {
                        Image image = global::Enlightenment_Presentation.Properties.Resources.jackClubs;
                        this.MakeRectangleImage(e, image);
                    } else if(this.currCard.suit==(int)Deck.cardSuit.diamond) {
                        Image image = global::Enlightenment_Presentation.Properties.Resources.jackDiamonds;
                        this.MakeRectangleImage(e, image);
                    } else if(this.currCard.suit==(int)Deck.cardSuit.heart) {
                        Image image = global::Enlightenment_Presentation.Properties.Resources.jackHearts;
                        this.MakeRectangleImage(e, image);
                    } else if(this.currCard.suit==(int)Deck.cardSuit.spade) {
                        Image image = global::Enlightenment_Presentation.Properties.Resources.jackSpades;
                        this.MakeRectangleImage(e, image);
                    }
                    break;

                //Kings Facings -------------------------------------
                case (int)Deck.cardValue.king:
                    if(this.currCard.suit==(int)Deck.cardSuit.club) {
                        Image image = global::Enlightenment_Presentation.Properties.Resources.kingClubs;
                        this.MakeRectangleImage(e, image);
                    } else if(this.currCard.suit==(int)Deck.cardSuit.diamond) {
                        Image image = global::Enlightenment_Presentation.Properties.Resources.kingDiamonds;
                        this.MakeRectangleImage(e, image);
                    } else if(this.currCard.suit==(int)Deck.cardSuit.heart) {
                        Image image = global::Enlightenment_Presentation.Properties.Resources.kingHearts;
                        this.MakeRectangleImage(e, image);
                    } else if(this.currCard.suit==(int)Deck.cardSuit.spade) {
                        Image image = global::Enlightenment_Presentation.Properties.Resources.kingSpades;
                        this.MakeRectangleImage(e, image);
                    }
                    break;
                    
                //Queen Facings -------------------------------------
                case (int)Deck.cardValue.queen:
                    if(this.currCard.suit==(int)Deck.cardSuit.club) {
                        Image image = global::Enlightenment_Presentation.Properties.Resources.queenClubs;
                        this.MakeRectangleImage(e, image);
                    } else if(this.currCard.suit==(int)Deck.cardSuit.diamond) {
                        Image image = global::Enlightenment_Presentation.Properties.Resources.queenDiamonds;
                        this.MakeRectangleImage(e, image);
                    } else if(this.currCard.suit==(int)Deck.cardSuit.heart) {
                        Image image = global::Enlightenment_Presentation.Properties.Resources.queenHearts;
                        this.MakeRectangleImage(e, image);
                    } else if(this.currCard.suit==(int)Deck.cardSuit.spade) {
                        Image image = global::Enlightenment_Presentation.Properties.Resources.queenSpades;
                        this.MakeRectangleImage(e, image);
                    }
                    break;

            } //end switch
        
        } //end function

        // =====================================



    } //EOC

} //EON
