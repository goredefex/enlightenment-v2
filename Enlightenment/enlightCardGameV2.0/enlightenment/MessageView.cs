﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace Enlightenment_Presentation {
    public class MessageView : RectangleView {

        //Message View Sizing
        private const int messageBoxWidth = 360;
        private const int messageBoxHeight = 110;
        private const int messageWidth = 215;
        private const int messageHeight = 80;
        private const int leftPadding = 15;
        private const int topPadding = 30;
        private const int wordWrapLength = 45;

        //Message Decals
        private Font font = new Font("Microsoft Sans Serif", 10, FontStyle.Regular);

        //Message Info
        private string message = String.Empty;
        

        // =====================================
        //Constructs ---------------------------
        // =====================================

        public MessageView(Point loc) { //<--Default
            this.setX(loc.X);
            this.setY(loc.Y);
            this.setWidth(messageBoxWidth);
            this.setHeight(messageBoxHeight);
            this.ChangeRectColor(Color.Black);
            this.ChangeWordColor(Color.White);
        
        } //end construct

        //Takes X,Y Coordinates
        public MessageView(int newX, int newY) {
            this.setX(newX);
            this.setY(newY);
            this.setWidth(messageBoxWidth);
            this.setHeight(messageBoxHeight);
            this.ChangeRectColor(Color.Black);
            this.ChangeWordColor(Color.White);
        
        } //end construct

        //Control Object GC Disposition
        ~MessageView() { this.font.Dispose(); } //end destructor

        // =====================================
        

        // =====================================
        //Functions ----------------------------
        // =====================================

        /// <summary>
        /// Draws The Message Box
        /// </summary>
        /// <param name="e">PaintEventArgs - Form Paint Event</param>
        public void DrawBox(System.Windows.Forms.PaintEventArgs e) {
            this.MakeRectangle(e);
            this.MakeAndSetWords(e);
        
        } //end function


        /// <summary>
        /// Sets The Message Used Within
        /// The MEssage Box
        /// </summary>
        /// <param name="newMsg">string - Message To Add</param>
        public void SetMessage(String newMsg) {
            if(newMsg.Length>=wordWrapLength) {
                for(int i=wordWrapLength; i<newMsg.Length; i=i+wordWrapLength) {
                    newMsg = newMsg.Insert(i, "\n");
                }
            }
            this.message = newMsg;
        
        } //end function


        /// <summary>
        /// Makes & Sets Message View
        /// </summary>
        /// <param name="e">PaintEventArgs - Form Paint Event</param>
        public void MakeAndSetWords(System.Windows.Forms.PaintEventArgs e) {
            this.MakeString(e, this.font, this.message, 
                (this.getX()+leftPadding), (this.getY()+topPadding));
        
        } //end function

        // =====================================



    } //EOC

} //EON
