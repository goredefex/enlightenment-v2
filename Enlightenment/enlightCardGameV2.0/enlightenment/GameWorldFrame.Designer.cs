﻿namespace Enlightenment_Presentation {
    partial class GameWorldFrame {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GameWorldFrame));
            this.frameRateTimer = new System.Windows.Forms.Timer(this.components);
            this.staticSoulLabel = new System.Windows.Forms.Label();
            this.staticDirectionLabel = new System.Windows.Forms.Label();
            this.userPlayDisplay = new System.Windows.Forms.Label();
            this.turnDisplayLabel = new System.Windows.Forms.Label();
            this.cardPlayedDispLbl = new System.Windows.Forms.Label();
            this.staticLevelLabel = new System.Windows.Forms.Label();
            this.wisdomDisplayLabel = new System.Windows.Forms.Label();
            this.staticWisdomLabel = new System.Windows.Forms.Label();
            this.staticTurnLabel = new System.Windows.Forms.Label();
            this.soulDisplayLabel = new System.Windows.Forms.Label();
            this.greedDisplayLabel = new System.Windows.Forms.Label();
            this.powerDisplayLabel = new System.Windows.Forms.Label();
            this.spadePictureBox = new System.Windows.Forms.PictureBox();
            this.aiPlayLabel = new System.Windows.Forms.Label();
            this.levelDisplayLabel = new System.Windows.Forms.Label();
            this.staticGreedLabel = new System.Windows.Forms.Label();
            this.staticPowerLabel = new System.Windows.Forms.Label();
            this.directionDisplayLabel = new System.Windows.Forms.Label();
            this.staticAIPlayedLabel = new System.Windows.Forms.Label();
            this.heartPictureBox = new System.Windows.Forms.PictureBox();
            this.clubPictureBox = new System.Windows.Forms.PictureBox();
            this.diamondPictureBox = new System.Windows.Forms.PictureBox();
            this.functionalityTimer = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.spadePictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.heartPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clubPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.diamondPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // frameRateTimer
            // 
            this.frameRateTimer.Interval = 45;
            this.frameRateTimer.Tick += new System.EventHandler(this.frameRateTimer_Tick);
            // 
            // staticSoulLabel
            // 
            this.staticSoulLabel.AutoSize = true;
            this.staticSoulLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticSoulLabel.Location = new System.Drawing.Point(714, 51);
            this.staticSoulLabel.Name = "staticSoulLabel";
            this.staticSoulLabel.Size = new System.Drawing.Size(55, 20);
            this.staticSoulLabel.TabIndex = 91;
            this.staticSoulLabel.Text = "Soul :";
            // 
            // staticDirectionLabel
            // 
            this.staticDirectionLabel.AutoSize = true;
            this.staticDirectionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticDirectionLabel.Location = new System.Drawing.Point(160, 569);
            this.staticDirectionLabel.Name = "staticDirectionLabel";
            this.staticDirectionLabel.Size = new System.Drawing.Size(86, 20);
            this.staticDirectionLabel.TabIndex = 100;
            this.staticDirectionLabel.Text = "Direction:";
            // 
            // userPlayDisplay
            // 
            this.userPlayDisplay.AutoSize = true;
            this.userPlayDisplay.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.userPlayDisplay.Location = new System.Drawing.Point(16, 191);
            this.userPlayDisplay.Name = "userPlayDisplay";
            this.userPlayDisplay.Size = new System.Drawing.Size(97, 20);
            this.userPlayDisplay.TabIndex = 97;
            this.userPlayDisplay.Text = "You Played: ";
            // 
            // turnDisplayLabel
            // 
            this.turnDisplayLabel.AutoSize = true;
            this.turnDisplayLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.turnDisplayLabel.Location = new System.Drawing.Point(61, 163);
            this.turnDisplayLabel.Name = "turnDisplayLabel";
            this.turnDisplayLabel.Size = new System.Drawing.Size(18, 20);
            this.turnDisplayLabel.TabIndex = 96;
            this.turnDisplayLabel.Text = "1";
            // 
            // cardPlayedDispLbl
            // 
            this.cardPlayedDispLbl.AutoSize = true;
            this.cardPlayedDispLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cardPlayedDispLbl.Location = new System.Drawing.Point(108, 191);
            this.cardPlayedDispLbl.Name = "cardPlayedDispLbl";
            this.cardPlayedDispLbl.Size = new System.Drawing.Size(92, 20);
            this.cardPlayedDispLbl.TabIndex = 80;
            this.cardPlayedDispLbl.Text = "Game Start";
            // 
            // staticLevelLabel
            // 
            this.staticLevelLabel.AutoSize = true;
            this.staticLevelLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticLevelLabel.Location = new System.Drawing.Point(20, 569);
            this.staticLevelLabel.Name = "staticLevelLabel";
            this.staticLevelLabel.Size = new System.Drawing.Size(61, 20);
            this.staticLevelLabel.TabIndex = 93;
            this.staticLevelLabel.Text = "Level :";
            // 
            // wisdomDisplayLabel
            // 
            this.wisdomDisplayLabel.AutoSize = true;
            this.wisdomDisplayLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.wisdomDisplayLabel.Location = new System.Drawing.Point(582, 52);
            this.wisdomDisplayLabel.Name = "wisdomDisplayLabel";
            this.wisdomDisplayLabel.Size = new System.Drawing.Size(18, 20);
            this.wisdomDisplayLabel.TabIndex = 90;
            this.wisdomDisplayLabel.Text = "0";
            // 
            // staticWisdomLabel
            // 
            this.staticWisdomLabel.AutoSize = true;
            this.staticWisdomLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticWisdomLabel.Location = new System.Drawing.Point(506, 51);
            this.staticWisdomLabel.Name = "staticWisdomLabel";
            this.staticWisdomLabel.Size = new System.Drawing.Size(82, 20);
            this.staticWisdomLabel.TabIndex = 89;
            this.staticWisdomLabel.Text = "Wisdom :";
            // 
            // staticTurnLabel
            // 
            this.staticTurnLabel.AutoSize = true;
            this.staticTurnLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticTurnLabel.Location = new System.Drawing.Point(16, 162);
            this.staticTurnLabel.Name = "staticTurnLabel";
            this.staticTurnLabel.Size = new System.Drawing.Size(49, 20);
            this.staticTurnLabel.TabIndex = 95;
            this.staticTurnLabel.Text = "Turn: ";
            // 
            // soulDisplayLabel
            // 
            this.soulDisplayLabel.AutoSize = true;
            this.soulDisplayLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.soulDisplayLabel.Location = new System.Drawing.Point(766, 51);
            this.soulDisplayLabel.Name = "soulDisplayLabel";
            this.soulDisplayLabel.Size = new System.Drawing.Size(18, 20);
            this.soulDisplayLabel.TabIndex = 92;
            this.soulDisplayLabel.Text = "0";
            // 
            // greedDisplayLabel
            // 
            this.greedDisplayLabel.AutoSize = true;
            this.greedDisplayLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.greedDisplayLabel.Location = new System.Drawing.Point(378, 51);
            this.greedDisplayLabel.Name = "greedDisplayLabel";
            this.greedDisplayLabel.Size = new System.Drawing.Size(18, 20);
            this.greedDisplayLabel.TabIndex = 88;
            this.greedDisplayLabel.Text = "0";
            // 
            // powerDisplayLabel
            // 
            this.powerDisplayLabel.AutoSize = true;
            this.powerDisplayLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.powerDisplayLabel.Location = new System.Drawing.Point(184, 51);
            this.powerDisplayLabel.Name = "powerDisplayLabel";
            this.powerDisplayLabel.Size = new System.Drawing.Size(18, 20);
            this.powerDisplayLabel.TabIndex = 86;
            this.powerDisplayLabel.Text = "0";
            // 
            // spadePictureBox
            // 
            this.spadePictureBox.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.spadePictureBox.Image = ((System.Drawing.Image)(resources.GetObject("spadePictureBox.Image")));
            this.spadePictureBox.Location = new System.Drawing.Point(40, 21);
            this.spadePictureBox.Name = "spadePictureBox";
            this.spadePictureBox.Size = new System.Drawing.Size(66, 87);
            this.spadePictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.spadePictureBox.TabIndex = 81;
            this.spadePictureBox.TabStop = false;
            // 
            // aiPlayLabel
            // 
            this.aiPlayLabel.AutoSize = true;
            this.aiPlayLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.aiPlayLabel.Location = new System.Drawing.Point(116, 223);
            this.aiPlayLabel.Name = "aiPlayLabel";
            this.aiPlayLabel.Size = new System.Drawing.Size(92, 20);
            this.aiPlayLabel.TabIndex = 99;
            this.aiPlayLabel.Text = "Game Start";
            // 
            // levelDisplayLabel
            // 
            this.levelDisplayLabel.AutoSize = true;
            this.levelDisplayLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.levelDisplayLabel.Location = new System.Drawing.Point(79, 569);
            this.levelDisplayLabel.Name = "levelDisplayLabel";
            this.levelDisplayLabel.Size = new System.Drawing.Size(19, 20);
            this.levelDisplayLabel.TabIndex = 94;
            this.levelDisplayLabel.Text = "0";
            // 
            // staticGreedLabel
            // 
            this.staticGreedLabel.AutoSize = true;
            this.staticGreedLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticGreedLabel.Location = new System.Drawing.Point(320, 51);
            this.staticGreedLabel.Name = "staticGreedLabel";
            this.staticGreedLabel.Size = new System.Drawing.Size(69, 20);
            this.staticGreedLabel.TabIndex = 87;
            this.staticGreedLabel.Text = "Greed :";
            // 
            // staticPowerLabel
            // 
            this.staticPowerLabel.AutoSize = true;
            this.staticPowerLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticPowerLabel.Location = new System.Drawing.Point(117, 51);
            this.staticPowerLabel.Name = "staticPowerLabel";
            this.staticPowerLabel.Size = new System.Drawing.Size(68, 20);
            this.staticPowerLabel.TabIndex = 85;
            this.staticPowerLabel.Text = "Power :";
            // 
            // directionDisplayLabel
            // 
            this.directionDisplayLabel.AutoSize = true;
            this.directionDisplayLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.directionDisplayLabel.Location = new System.Drawing.Point(250, 569);
            this.directionDisplayLabel.Name = "directionDisplayLabel";
            this.directionDisplayLabel.Size = new System.Drawing.Size(49, 20);
            this.directionDisplayLabel.TabIndex = 101;
            this.directionDisplayLabel.Text = "none";
            // 
            // staticAIPlayedLabel
            // 
            this.staticAIPlayedLabel.AutoSize = true;
            this.staticAIPlayedLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticAIPlayedLabel.Location = new System.Drawing.Point(16, 223);
            this.staticAIPlayedLabel.Name = "staticAIPlayedLabel";
            this.staticAIPlayedLabel.Size = new System.Drawing.Size(97, 20);
            this.staticAIPlayedLabel.TabIndex = 98;
            this.staticAIPlayedLabel.Text = "Last AI Play:";
            // 
            // heartPictureBox
            // 
            this.heartPictureBox.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.heartPictureBox.Image = ((System.Drawing.Image)(resources.GetObject("heartPictureBox.Image")));
            this.heartPictureBox.Location = new System.Drawing.Point(629, 21);
            this.heartPictureBox.Name = "heartPictureBox";
            this.heartPictureBox.Size = new System.Drawing.Size(66, 87);
            this.heartPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.heartPictureBox.TabIndex = 84;
            this.heartPictureBox.TabStop = false;
            // 
            // clubPictureBox
            // 
            this.clubPictureBox.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.clubPictureBox.Image = ((System.Drawing.Image)(resources.GetObject("clubPictureBox.Image")));
            this.clubPictureBox.Location = new System.Drawing.Point(425, 21);
            this.clubPictureBox.Name = "clubPictureBox";
            this.clubPictureBox.Size = new System.Drawing.Size(66, 87);
            this.clubPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.clubPictureBox.TabIndex = 83;
            this.clubPictureBox.TabStop = false;
            // 
            // diamondPictureBox
            // 
            this.diamondPictureBox.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.diamondPictureBox.Image = ((System.Drawing.Image)(resources.GetObject("diamondPictureBox.Image")));
            this.diamondPictureBox.Location = new System.Drawing.Point(234, 21);
            this.diamondPictureBox.Name = "diamondPictureBox";
            this.diamondPictureBox.Size = new System.Drawing.Size(66, 87);
            this.diamondPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.diamondPictureBox.TabIndex = 82;
            this.diamondPictureBox.TabStop = false;
            // 
            // functionalityTimer
            // 
            this.functionalityTimer.Interval = 120;
            this.functionalityTimer.Tick += new System.EventHandler(this.functionalityTimer_Tick);
            // 
            // GameWorldFrame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkGreen;
            this.ClientSize = new System.Drawing.Size(809, 598);
            this.Controls.Add(this.staticSoulLabel);
            this.Controls.Add(this.staticDirectionLabel);
            this.Controls.Add(this.userPlayDisplay);
            this.Controls.Add(this.turnDisplayLabel);
            this.Controls.Add(this.cardPlayedDispLbl);
            this.Controls.Add(this.staticLevelLabel);
            this.Controls.Add(this.wisdomDisplayLabel);
            this.Controls.Add(this.staticWisdomLabel);
            this.Controls.Add(this.staticTurnLabel);
            this.Controls.Add(this.soulDisplayLabel);
            this.Controls.Add(this.greedDisplayLabel);
            this.Controls.Add(this.powerDisplayLabel);
            this.Controls.Add(this.spadePictureBox);
            this.Controls.Add(this.aiPlayLabel);
            this.Controls.Add(this.levelDisplayLabel);
            this.Controls.Add(this.staticGreedLabel);
            this.Controls.Add(this.staticPowerLabel);
            this.Controls.Add(this.directionDisplayLabel);
            this.Controls.Add(this.staticAIPlayedLabel);
            this.Controls.Add(this.heartPictureBox);
            this.Controls.Add(this.clubPictureBox);
            this.Controls.Add(this.diamondPictureBox);
            this.DoubleBuffered = true;
            this.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(825, 636);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(825, 636);
            this.Name = "GameWorldFrame";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "GameWorldFrame";
            this.Load += new System.EventHandler(this.GameWorldFrame_Load);
            ((System.ComponentModel.ISupportInitialize)(this.spadePictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.heartPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clubPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.diamondPictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer frameRateTimer;
        private System.Windows.Forms.Label staticSoulLabel;
        private System.Windows.Forms.Label staticDirectionLabel;
        private System.Windows.Forms.Label userPlayDisplay;
        private System.Windows.Forms.Label turnDisplayLabel;
        private System.Windows.Forms.Label cardPlayedDispLbl;
        private System.Windows.Forms.Label staticLevelLabel;
        private System.Windows.Forms.Label wisdomDisplayLabel;
        private System.Windows.Forms.Label staticWisdomLabel;
        private System.Windows.Forms.Label staticTurnLabel;
        private System.Windows.Forms.Label soulDisplayLabel;
        private System.Windows.Forms.Label greedDisplayLabel;
        private System.Windows.Forms.Label powerDisplayLabel;
        private System.Windows.Forms.PictureBox spadePictureBox;
        private System.Windows.Forms.Label aiPlayLabel;
        private System.Windows.Forms.Label levelDisplayLabel;
        private System.Windows.Forms.Label staticGreedLabel;
        private System.Windows.Forms.Label staticPowerLabel;
        private System.Windows.Forms.Label directionDisplayLabel;
        private System.Windows.Forms.Label staticAIPlayedLabel;
        private System.Windows.Forms.PictureBox heartPictureBox;
        private System.Windows.Forms.PictureBox clubPictureBox;
        private System.Windows.Forms.PictureBox diamondPictureBox;
        private System.Windows.Forms.Timer functionalityTimer;
    }
}